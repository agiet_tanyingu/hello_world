import { Observable } from 'rxjs';

export abstract class HelloWorldServiceAbstract {
  public abstract get helloWorld(): Observable<string>;
}
