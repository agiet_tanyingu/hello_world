import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HelloWorldServiceAbstract } from './abstract/hello-world.service.abstract';

@Injectable({
  providedIn: 'root',
})
export class HelloWorldService implements HelloWorldServiceAbstract {
  constructor(private http: HttpClient) { }
  public get helloWorld(): Observable<string> {
      // const url: string = environment.helloWolrdUrl;
      const url: string = window['apiUrl']
      return this.http.get(url, { responseType: 'text'});

  }
}
