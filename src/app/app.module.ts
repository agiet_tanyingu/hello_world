import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HelloWorldComponent } from './components/hello-world/hello-world.component';
import { HelloWorldService } from './services/hello-world.service';
import { HelloWorldServiceAbstract } from './services/abstract/hello-world.service.abstract';

@NgModule({
  declarations: [AppComponent, HelloWorldComponent],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, HttpClientModule],
  providers: [{ provide: HelloWorldServiceAbstract, useClass: HelloWorldService }],
  bootstrap: [AppComponent],
})
export class AppModule {}
