import { Component } from '@angular/core';
import { HelloWorldServiceAbstract } from 'src/app/services/abstract/hello-world.service.abstract';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-hello-world',
  templateUrl: './hello-world.component.html',
  styleUrls: ['./hello-world.component.scss'],
})
export class HelloWorldComponent {
  public helloWorld$: Observable<string>;
  constructor(private helloWorldService: HelloWorldServiceAbstract) {}


  public getResponse(): void {
    this.helloWorld$ = this.helloWorldService.helloWorld;
  }
}
